import { Mongo } from 'meteor/mongo';
import { FieldsSchema, DefinitionsSchema, LayoutsSchema, DocumentsSchema} from './schema';

const Fields = new Mongo.Collection('fields');
Fields.attachSchema(FieldsSchema);

const Definitions = new Mongo.Collection('definitions');
Definitions.attachSchema(DefinitionsSchema);

const Layouts = new Mongo.Collection('layouts');
Layouts.attachSchema(LayoutsSchema);

const Documents = new Mongo.Collection('documents');
Documents.attachSchema(DocumentsSchema);

export { Fields, Definitions, Layouts, Documents };
