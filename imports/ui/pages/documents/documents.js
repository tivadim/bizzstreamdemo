import { Template } from 'meteor/templating';
import { Documents, Definitions, Layouts } from '../../../api/entities';

import './documents.html';

Template.DocumentsList.helpers({
    layout() {
        let definition = Definitions.findOne();
        if (!definition) return null;

        let layout = Layouts.findOne({_id: definition.layoutId});

        layout.header.rows.forEach(row => {
            row.columns.forEach(col => {
                let field = definition.schema.fields.find(field => {
                    if (field._id === col.fieldId) return field;
                });
                Object.assign(col, col, field);
            })
        });

        return layout;
    },

    documents() {
        return Documents.find();
    },
});

Template.DocumentsList.events({
    'submit .new-document'(event) {
        event.preventDefault();

        let fields = [];
        let formFields = $("input.document-field");
        _.each(formFields, function(formField) {
            let field =  {
                _id: formField.id,
                label: formField.dataset.label,
                name: formField.dataset.name,
                type: formField.type,
                value: formField.value
            };
            if (formField.maxLength) field.maxLength = formField.maxLength;

            fields.push(field);
            formField.value = '';
        });

        Documents.insert({ fields: fields });
    },
    'click .delete-document'() {
        Documents.remove(this._id);
    },

});



