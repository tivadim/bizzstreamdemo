import { Template } from 'meteor/templating';
import { Fields } from '../../../../api/entities';

import './fields.html';

Template.FieldsList.helpers({
    fields() {
        return Fields.find();
    },
});