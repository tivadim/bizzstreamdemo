import { Template } from 'meteor/templating';
import { Fields, Layouts } from '../../../../api/entities';

import './layouts.html';

Template.LayoutsList.helpers({
    layouts() {
        return Layouts.find();
    },

    fieldNameById(id) {
        return Fields.findOne({_id: id}).label;
    }

});